package GUI;

import java.awt.*;
import javax.swing.*;

public class GUI_02 {


	  public static void main(String[] args) {
	    JFrame frame = new JFrame();
	    JButton button = new JButton("Press me");
	    frame.setLayout(new BorderLayout());
	    frame.add(button, BorderLayout.CENTER);
	    button.addActionListener(
	        event -> System.out.println("Button says: " + 
	                   event.getActionCommand()));
	    frame.pack();
	    frame.setVisible(true);
	  }

}




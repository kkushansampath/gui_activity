package GUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GUI_01 {
	
	public static void main(String[] args) {
	    JFrame frame = new JFrame();
	    JButton button = new JButton("Press Me");
	    frame.setLayout(new BorderLayout());
	    frame.add(button, BorderLayout.CENTER);
	    
	    button.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent event) {
	        System.out.println("Button says: " + 
	          event.getActionCommand());        
	      }
	    });
	        
	    frame.pack();
	    frame.setVisible(true);
	  }

}
